<?php

Class model_todo extends Model
{

    /**
     * Create new category
     *
     * @param $title
     * @param $color
     * @param $id
     * @return array|PDOStatement
     */
    public function add_category($title, $color, $id)
    {
        $createCategory = $this->db()->prepare("INSERT INTO categories (user_id, name, color) VALUES (?, ?, ?)");
        $createCategory->bindParam(1, $id);
        $createCategory->bindParam(2, $title);
        $createCategory->bindParam(3, $color);
        $createCategory->execute();

        $allCategories = $this->db()->query("SELECT * FROM categories WHERE user_id='" . $id . "'");
        $allCategories = $allCategories->fetchAll(PDO::FETCH_CLASS);
        return $allCategories;
    }

    /**
     * Get user categories
     *
     * @param $user_id
     * @return array|PDOStatement
     */
    public function get_categories($user_id)
    {
        $categories = $this->db()->query("SELECT * FROM categories WHERE user_id='" . $user_id . "'");
        $categories = $categories->fetchAll(PDO::FETCH_CLASS);
        return $categories;
    }

    /**
     * Edit user category
     *
     * @param $user_id
     * @param $category_id
     * @param $name
     * @param $color
     * @return array|PDOStatement
     */
    public function edit_category($user_id, $category_id, $name, $color)
    {
        $this->db()->query("UPDATE categories SET name = '" . $name . "', color = '" . $color . "' WHERE categories.id='" . $category_id . "' AND categories.user_id='" . $user_id . "'");
        return $this->get_categories($user_id);
    }

    /**
     * Delete user category, if it does not have uncompleted goals
     *
     * @param $user_id
     * @param $category_id
     * @return array|PDOStatement|void
     */
    public function delete_category($user_id, $category_id)
    {
        $checkGoals = $this->db()->query("SELECT goals.*, categories.name FROM goals INNER JOIN categories ON goals.category_id = categories.id WHERE goals.category_id = '" . $category_id . "' AND goals.user_id='" . $user_id . "' AND goals.checked = 0");
        $checkGoals = $checkGoals->fetchAll(PDO::FETCH_CLASS);
        if (count($checkGoals)) return die();
        else {
            $this->db()->query("DELETE FROM categories WHERE categories.id='" . $category_id . "'");
            return $this->get_categories($user_id);
        }
    }

    /**
     * Create new goal
     *
     * @param $user_id
     * @param $category_id
     * @param $title
     * @param $date
     * @param $priority
     * @return array|PDOStatement
     */
    public function add_goal($user_id, $category_id, $title, $date, $priority)
    {
        $createGoal = $this->db()->prepare("INSERT INTO goals (user_id, category_id, title, date, priority) VALUES (?, ?, ?, ?, ?)");
        $createGoal->bindParam(1, $user_id);
        $createGoal->bindParam(2, $category_id);
        $createGoal->bindParam(3, $title);
        $createGoal->bindParam(4, $date);
        $createGoal->bindParam(5, $priority);
        $createGoal->execute();

        $allGoals = $this->get_goals($user_id, $category_id);
        return $allGoals;
    }

    /**
     * Get user goals
     *
     * @param $user_id
     * @param null $category_id
     * @return array|PDOStatement
     */
    public function get_goals($user_id, $category_id = null)
    {
        $goals = $this->db()->query("SELECT goals.*, categories.name FROM goals INNER JOIN categories ON goals.category_id = categories.id WHERE goals.user_id='" . $user_id . "' AND goals.checked = 0 ORDER BY date");
        if ($category_id) $goals = $this->db()
            ->query("SELECT goals.*, categories.name FROM goals LEFT JOIN categories ON goals.category_id = categories.id WHERE goals.category_id = '" . $category_id . "' AND goals.user_id='" . $user_id . "' AND goals.checked = 0 ORDER BY date");
        $goals = $goals->fetchAll(PDO::FETCH_CLASS);
        return $goals;
    }

    /**
     * Get user goals by time
     *
     * @param $user_id
     * @param bool $week
     * @return array|PDOStatement
     */
    public function get_goals_by_time($user_id, $week = false)
    {
        $goals = $this->db()->query("SELECT goals.*, categories.name FROM goals LEFT JOIN categories ON goals.category_id = categories.id WHERE goals.user_id='" . $user_id . "' AND goals.date='" . date('Y-m-d') . "' AND goals.checked = 0 ORDER BY date");
        if ($week) $goals = $this->db()->query("SELECT goals.*, categories.name FROM goals LEFT JOIN categories ON goals.category_id = categories.id WHERE goals.user_id='" . $user_id . "' AND goals.date<'" . date('Y-m-d', strtotime("+7 day")) . "' AND goals.checked = 0 ORDER BY date");
        $goals = $goals->fetchAll(PDO::FETCH_CLASS);
        return $goals;
    }

    /**
     * Get user completed goals
     *
     * @param $user_id
     * @return array|PDOStatement
     */
    public function get_checked_goals($user_id)
    {
        $goals = $this->db()->query("SELECT goals.*, categories.name FROM goals LEFT JOIN categories ON goals.category_id = categories.id WHERE goals.user_id='" . $user_id . "' AND goals.checked = 1 ORDER BY date");
        $goals = $goals->fetchAll(PDO::FETCH_CLASS);
        return $goals;
    }

    /**
     * Mark user goal as checked
     *
     * @param $user_id
     * @param $goal_id
     * @return array|PDOStatement
     */
    public function check_goal($user_id, $goal_id)
    {
        $this->db()->query("UPDATE goals SET checked = '1' WHERE goals.id='" . $goal_id . "'");
        return $this->get_checked_goals($user_id);
    }

    /**
     * Update user goal
     *
     * @param $user_id
     * @param $goal_id
     * @param $category_id
     * @param $title
     * @param $date
     * @param $priority
     * @return array|PDOStatement
     */
    public function edit_goal($user_id, $goal_id, $category_id, $title, $date, $priority)
    {
        $this->db()->query("UPDATE goals SET category_id = '" . $category_id . "', title = '" . $title . "', date='" . $date . "', priority='" . $priority . "' WHERE goals.id='" . $goal_id . "' AND goals.user_id='" . $user_id . "'");
        return $this->get_goals($user_id);
    }

    /**
     * Delete user goal
     *
     * @param $user_id
     * @param $goal_id
     * @return array|PDOStatement
     */
    public function delete_goal($user_id, $goal_id)
    {
        $this->db()->query("DELETE FROM goals WHERE goals.id='" . $goal_id . "'");
        return $this->get_goals($user_id);
    }
}