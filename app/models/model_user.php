<?php

Class model_user extends Model
{

    /**
     * Create new user
     *
     * @param $data
     * @return stdClass
     */
    public function register($data)
    {
        $db = $this->db();
        $checkUnique = $db->query("SELECT username FROM users WHERE username='" . $data['username'] . "'");
        $checkUnique = $checkUnique->fetch();

        if (!empty($checkUnique)) die('Username must be unique');

        $createUser = $db->prepare("INSERT INTO users (username, email, password) VALUES (?, ?, ?)");

        $pass = password_hash($data['password'], PASSWORD_DEFAULT);

        $createUser->bindParam(1, $data['username']);
        $createUser->bindParam(2, $data['email']);
        $createUser->bindParam(3, $pass);
        $createUser->execute();

        $User = new stdClass();
        $User->id = $db->lastInsertId();
        $User->username = $data['username'];
        $User->email = $data['email'];

        return $User;
    }

    /**
     * Check user
     *
     * @param $data
     * @return stdClass
     */
    public function login($data)
    {
        $getUser = $this->db()->query("SELECT * FROM users WHERE username='" . $data['username'] . "'");
        $getUser = $getUser->fetch();

        if (empty($getUser)) die('User does not exist');

        if (!password_verify($data['password'], $getUser['password'])) die('Password mismatch');

        $User = new stdClass();
        $User->id = $getUser['id'];
        $User->username = $getUser['username'];
        $User->email = $getUser['email'];

        return $User;
    }

}