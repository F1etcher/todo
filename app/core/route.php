<?php

Class Route
{

    /**
     * Main routing, finding controller, model, action, attach it if found
     */
    static function start()
    {
        // Default controller and action
        $controller_name = 'Site';
        $action_name = 'index';

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($routes[1])) $controller_name = $routes[1]; // Get controller name

        if (!empty($routes[2])) $action_name = $routes[2]; // Get action name

        // Add prefixes
        $model_name = 'Model_' . $controller_name;
        $controller_name = 'Controller_' . $controller_name;
        $action_name = 'action_' . $action_name;

        // Attach model file (if it exist)
        $model_file = strtolower($model_name) . '.php';
        $model_path = "app/models/" . $model_file;

        if (file_exists($model_path)) include "app/models/" . $model_file;

        // Attach controller file
        $controller_file = strtolower($controller_name) . '.php';
        $controller_path = "app/controllers/" . $controller_file;

        if (file_exists($controller_path)) include "app/controllers/" . $controller_file;
        else Route::ErrorPage404(); // Return 404, if controller not found

        // Creating new controller
        $controller = new $controller_name;
        $action = $action_name;

        if (method_exists($controller, $action)) $controller->$action(); // Init controller method
        else Route::ErrorPage404(); // Return 404, if action not found
    }

    /**
     * Throw 404 error
     */
    static function ErrorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
        die();
    }

}