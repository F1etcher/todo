<?php

class Controller
{

    public $model;
    public $view;

    /**
     * Return new view instance
     *
     * Controller constructor.
     */
    function __construct()
    {
        $this->view = new View();
    }

    /**
     * Default action
     */
    function action_index()
    {

    }

}