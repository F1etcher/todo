<?php

class View
{

    /**
     * Generate dynamic view
     *
     * @param $content_view
     * @param $template_view
     * @param null $data
     */
    function generate($content_view, $template_view, $data = null)
    {
        include 'app/views/' . $template_view;
    }

}