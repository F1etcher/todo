<?php

class Model
{
    /**
     * Database connection
     *
     * @return PDO
     */
    public function db()
    {
        $dsn = 'mysql:host='.getenv('DB_host').';dbname=' . getenv('DB_name') . '';
        $user = getenv('DB_user');
        $password = getenv('DB_password');

        $db = new PDO($dsn, $user, $password);
        return $db;
    }

    public function get_data()
    {

    }

}