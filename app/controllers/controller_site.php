<?php

include 'app/models/model_todo.php';

class Controller_site extends Controller
{

    /**
     * Return 'login/registration' page.
     */
    function action_index()
    {
        $this->view->generate('pages/login_view.php', 'layouts/template_view.php', [
            'title' => 'Login',
            'css' => [
                '/public/css/pages/login.css'
            ],
            'js' => [
                '/public/js/pages/login.js'
            ]
        ]);
    }

    /**
     * Return 'dashboard' page.
     */
    function action_dashboard()
    {
        session_start();
        if (!isset($_SESSION['User'])) header('Location: /');

        $model_todo = new model_todo();
        $categories = $model_todo->get_categories($_SESSION['User']->id);
        $goals = $model_todo->get_goals($_SESSION['User']->id);

        $this->view->generate('pages/dashboard_view.php', 'layouts/template_view.php', [
            'title' => 'Dashboard',
            'css' => [
                'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.min.css',
                'https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.5.5/datepicker.min.css',
                'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
                'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css',
                '/public/css/pages/dashboard.css'
            ],
            'js' => [
                'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.5.5/datepicker.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
                '/public/js/pages/dashboard.js'
            ],
            'categories' => $categories,
            'goals' => $goals
        ]);
    }

}