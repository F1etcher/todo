<?php

Class Controller_User extends Controller
{

    /**
     * User login
     */
    public function action_login()
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') die('Must be POST');
        if (empty($_POST['username']) || empty($_POST['password'])) header('Location: /');
        $user_model = new model_user();
        $User = $user_model->login([
            'username' => trim($_POST['username']),
            'password' => trim($_POST['password'])
        ]);

        session_start();
        $_SESSION['User'] = $User;

        header('Location: /site/dashboard');
    }

    /**
     * User registration
     */
    public function action_register()
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') die('Must be POST');
        if (empty($_POST['username']) || empty($_POST['email']) || empty($_POST['password'])) header('Location: /');
        $user_model = new model_user();
        $User = $user_model->register([
            'username' => trim($_POST['username']),
            'email' => trim($_POST['email']),
            'password' => trim($_POST['password'])
        ]);

        session_start();
        $_SESSION['User'] = $User;

        header('Location: /site/dashboard');
    }

}