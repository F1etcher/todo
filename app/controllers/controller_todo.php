<?php

class Controller_todo extends Controller
{

    /**
     * Check current user, request method and request params
     *
     * @param null $data
     * @return bool
     */
    private function check_ajax($data = null)
    {
        session_start();
        if (!isset($_SESSION['User']) && $_SERVER['REQUEST_METHOD'] !== 'POST') die();

        if ($data) {
            foreach ($data as $item) {
                if (empty($item)) die();
            }
        }

        return true;
    }

    /**
     * Create category
     */
    public function action_add_category()
    {
        $this->check_ajax([
            $_POST['title'],
            $_POST['color']
        ]);

        $model_todo = new model_todo();
        $list = $model_todo->add_category($_POST['title'], $_POST['color'], $_SESSION['User']->id);

        echo json_encode($list);
    }

    /**
     * Edit category
     */
    public function action_edit_category()
    {
        $this->check_ajax([
            $_POST['category_id'],
            $_POST['title'],
            $_POST['color']
        ]);

        $model_todo = new model_todo();
        $goals = $model_todo->edit_category($_SESSION['User']->id, $_POST['category_id'], $_POST['title'], $_POST['color']);

        echo json_encode($goals);
    }

    /**
     * Delete category
     */
    public function action_delete_category()
    {
        $this->check_ajax([
            $_POST['category_id']
        ]);

        $model_todo = new model_todo();
        $goals = $model_todo->delete_category($_SESSION['User']->id, $_POST['category_id']);

        echo json_encode($goals);
    }

    /**
     * Create goal
     */
    public function action_add_goal()
    {
        $this->check_ajax([
            $_POST['title'],
            $_POST['priority'],
            $_POST['category'],
            $_POST['date']
        ]);

        $model_todo = new model_todo();
        $list = $model_todo->add_goal($_SESSION['User']->id, $_POST['category'], $_POST['title'], $_POST['date'], $_POST['priority']);

        echo json_encode($list);
    }

    /**
     * Get current user goals (all or by category)
     */
    public function action_get_goals()
    {
        $this->check_ajax();

        $model_todo = new model_todo();

        $category_id = 0;
        if (isset($_POST['category_id'])) $category_id = $_POST['category_id'];
        $goals = $model_todo->get_goals($_SESSION['User']->id, $category_id);

        echo json_encode($goals);
    }

    /**
     * Get current user goals by time
     */
    public function action_get_goals_by_time()
    {
        $this->check_ajax();

        $model_todo = new model_todo();
        $goals = $model_todo->get_goals_by_time($_SESSION['User']->id);

        if ($_POST['week']) $goals = $model_todo->get_goals_by_time($_SESSION['User']->id, true);

        echo json_encode($goals);
    }

    /**
     * Mark goal as completed
     */
    public function action_check_goal()
    {
        $this->check_ajax([
            $_POST['goal_id']
        ]);

        $model_todo = new model_todo();
        $goals = $model_todo->check_goal($_SESSION['User']->id, $_POST['goal_id']);

        echo json_encode($goals);
    }

    /**
     * Get current user completed goals
     */
    public function action_get_checked_goals()
    {
        $this->check_ajax();

        $model_todo = new model_todo();
        $goals = $model_todo->get_checked_goals($_SESSION['User']->id);

        echo json_encode($goals);
    }

    /**
     * Edit goal
     */
    public function action_edit_goal()
    {
        $this->check_ajax([
            $_POST['goal_id'],
            $_POST['category_id'],
            $_POST['title'],
            $_POST['date'],
            $_POST['priority']
        ]);

        $model_todo = new model_todo();
        $goals = $model_todo->edit_goal($_SESSION['User']->id, $_POST['goal_id'], $_POST['category_id'], $_POST['title'], $_POST['date'], $_POST['priority']);

        echo json_encode($goals);
    }

    /**
     * Delete goal
     */
    public function action_delete_goal()
    {
        $this->check_ajax([
            $_POST['goal_id']
        ]);

        $model_todo = new model_todo();
        $goals = $model_todo->delete_goal($_SESSION['User']->id, $_POST['goal_id']);

        echo json_encode($goals);
    }

}