<nav class="navbar navbar-inverse bg-inverse top-menu">
    <a class="navbar-brand" href="/site/dashboard">Dashboard</a>
    <button class="btn btn-secondary" id="checked" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-ban" aria-hidden="true"></i>
    </button>
</nav> <!-- navigation end -->

<div class="col-md-12">

    <div class="col-md-2">
        <ul class="nav sidebar-nav">
            <li id="todayGoals"><span style="background-color: #ec1b5a;"></span><a href="#">Today</a></li>
            <li id="getWeeklyGoals"><span style="background-color: #ec1b5a;"></span><a href="#">Next 7 days</a></li>
        </ul>
        <h3>Categories</h3>
        <ul class="nav sidebar-nav categories">
            <?php foreach ($data['categories'] as $category) {
                echo '<li data-id="' . $category->id . '" class="category">
                        <span style="background-color: ' . $category->color . ';"></span>
                        <a href="#" class="col-md-2">' . $category->name . '</a>
                        <div class="dropdown col-md-2">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item edit-category" href="#" data-toggle="modal" data-target="#modalWindowCategories">Edit</a>
                                <a class="dropdown-item delete-category" href="#">Delete</a>
                            </div>
                        </div>
                    </li>';
            } ?>
        </ul>
        <div class="new-category">
            <div class="input-group">
                <input type="text" name="category" placeholder="New category" class="col-md-8">
                <div class="cp9 input-group colorpicker-component"><input type="hidden" name="color" value="#00AABB"
                                                                          class="form-control"/> <span
                            class="input-group-addon"><i></i></span></div>
            </div>
            <button class="btn btn-success" id="addCategory">Add</button>
        </div>
    </div> <!-- sidebar end -->

    <div class="col-md-10">
        <h1>Goals</h1>
        <ul class="list-group goals">
            <?php foreach ($data['goals'] as $goal) {

                $color = 'white';
                if (+$goal->priority === 1) $color = 'red';
                if (+$goal->priority === 2) $color = 'orange';

                echo '<li class="list-group-item list-group-item-dark" data-date="' . $goal->date . '" data-id="' . $goal->id . '">
                        <span class="color" style="background-color: ' . $color . '"></span>
                        <p class="goal col-md-4">' . $goal->title . '</p>
                        <p class="category col-md-4">Category: ' . $goal->name . '</p>
                        <div class="control dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item check-goal" href="#">Done</a>
                                <a class="dropdown-item edit-goal" href="#" data-toggle="modal" data-target="#modalWindowGoals">Edit</a>
                                <a class="dropdown-item delete-goal" href="#">Delete</a>
                            </div>
                        </div>
                    </li>';
            } ?>
        </ul>
        <div class="form-group new-goal">
            <div class="form-group col-md-12">
                <input type="text" name="title" placeholder="New goal" class="col-md-10">
                <div class="input-group col-md-2">
                    <input class="form-control docs-date datepicker" name="date" id="datepicker"
                           placeholder="Pick a date"
                           type="text">
                    <span class="input-group-btn">
                  <button type="button" class="btn btn-default docs-datepicker-trigger" disabled>
                    <i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
                  </button>
                </span>
                </div>
            </div>
            <div class="form-group col-md-1">
                <button class="btn btn-success" id="addGoal">Add</button>
            </div>
            <div class="form-group" align="right">
                <select class="selectPicker col-md-2" data-show-icon="true" name="priority">
                    <option data-content="<i class='fa fa-bolt' aria-hidden='true'></i>" selected disabled></option>
                    <option value="1">High</option>
                    <option value="2">Medium</option>
                    <option value="3">Low</option>
                </select>
                <select class="selectPicker col-md-2" data-show-icon="true" name="category" id="chooseCat">
                    <option data-content="<i class='fa fa-smile-o' aria-hidden='true'></i>" selected disabled></option>
                    <?php foreach ($data['categories'] as $category) {
                        echo '<option value="' . $category->id . '">' . $category->name . '</option>';
                    } ?>
                </select>
            </div>
        </div>
    </div> <!-- content -->

</div>

<!-- Modal for goals -->
<div class="modal fade" id="modalWindowGoals" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-group new-goal">
                    <div class="form-group col-md-12">
                        <input type="hidden" value="" name="goal_id">
                        <input type="text" name="title" placeholder="" class="col-md-8">
                        <div class="input-group col-md-4">
                            <input class="form-control docs-date datepicker" name="date" id="datepickerModal"
                                   placeholder="Pick a date" type="text">
                            <span class="input-group-btn">
                  <button type="button" class="btn btn-default docs-datepicker-trigger" disabled>
                    <i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
                  </button>
                </span>
                        </div>
                    </div>
                    <div class="form-group col-md-1">
                        <button class="btn btn-success" id="updateGoal">Update</button>
                    </div>
                    <div class="form-group" align="right">
                        <select class="selectPicker col-md-2" data-show-icon="true" name="priority">
                            <option data-content="<i class='fa fa-bolt' aria-hidden='true'></i>" selected
                                    disabled></option>
                            <option value="1">High</option>
                            <option value="2">Medium</option>
                            <option value="3">Low</option>
                        </select>
                        <select class="selectPicker col-md-2" data-show-icon="true" name="category"  id="chooseCatModal">
                            <option data-content="<i class='fa fa-smile-o' aria-hidden='true'></i>" selected
                                    disabled></option>
                            <?php foreach ($data['categories'] as $category) {
                                echo '<option value="' . $category->id . '">' . $category->name . '</option>';
                            } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<!-- Modal for categories -->
<div class="modal fade" id="modalWindowCategories" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="new-category">
                    <div class="">
                        <input type="hidden" name="category_id" value="">
                        <input type="text" name="category" placeholder="New category" class="col-md-10">
                        <div class="cp9 input-group colorpicker-component"><input type="hidden" name="color"
                                                                                  value="#00AABB" class="form-control"/>
                            <span class="input-group-addon"><i></i></span></div>
                    </div>
                    <button class="btn btn-success" id="updateCategory">Update</button>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>