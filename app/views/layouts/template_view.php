<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $data['title']; ?></title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <?php if (isset($data['css'])) {
        foreach ($data['css'] as $css) {
            echo '<link rel="stylesheet" href="' . $css . '">' . "\n";
        }
    } ?>
</head>
<body>
<?php include 'app/views/' . $content_view; ?>

<!-- Jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php if (isset($data['js'])) {
    foreach ($data['js'] as $js) {
        echo '<script src="' . $js . '"></script>' . "\n";
    }
} ?>
</body>
</html>