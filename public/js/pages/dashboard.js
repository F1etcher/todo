$(document).ready(function () {

    $('.cp9').colorpicker();

    $(".datepicker").datepicker({
        format: 'YYYY-MM-DD'
    });

    $('.selectPicker').selectpicker();

    function addGoal(value) {
        var color = 'white';
        var node = '';
        if (!+value.checked) node = '<a class="dropdown-item check-goal" href="#">Done</a>';
        if (+value.priority === 1) color = 'red';
        if (+value.priority === 2) color = 'orange';

        $('.goals').append(
            '<li class="list-group-item list-group-item-dark" data-date="' + value.date + '" data-id="' + value.id + '">\n' +
            '<span class="color" style="background-color: ' + color + '"></span>\n' +
            '<p class="goal col-md-4">' + value.title + '</p>\n' +
            '<p class="category col-md-4">Category: ' + value.name + '</p>\n' +
            '<div class="control dropdown">\n' +
            '   <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
            '       <i class="fa fa-ellipsis-v" aria-hidden="true"></i>\n' +
            '   </button>\n' +
            '   <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">\n' +
            '       ' + node + '\n' +
            '       <a class="dropdown-item edit-goal" href="#" data-toggle="modal" data-target="#modalWindowGoals">Edit</a>\n' +
            '       <a class="dropdown-item delete-goal" href="#">Delete</a>\n' +
            '   </div>\n' +
            '</div></li>'
        );

        bindGoalsControl();
    }

    function addCategory(value) {

        $('.categories').append('' +
            '<li class="category" data-id="' + value.id + '">' +
            '<span style="background-color: ' + value.color + ';"></span>' +
            '<a href="#">' + value.name + '</a>' +
            '<div class="dropdown col-md-2">\n' +
            '<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>\n' +
            '</button>\n' +
            '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">\n' +
            '<a class="dropdown-item edit-category" href="#" data-toggle="modal" data-target="#modalWindowCategories">Edit</a>\n' +
            '<a class="dropdown-item delete-category" href="#">Delete</a>\n' +
            '</div>\n' +
            '</div></li>');

        $('#chooseCat').append('<option value="' + value.id + '">' + value.name + '</option>').selectpicker('refresh');
        $('#chooseCatModal').append('<option value="' + value.id + '">' + value.name + '</option>').selectpicker('refresh');

        bindGoalsControl();
        bindCategoriesControl();

    }

    function getGoalsByTime(week) {
        $.ajax({
            url: '/todo/get_goals_by_time',
            method: 'POST',
            data: {
                week: week
            }
        }).done(function (msg) {
            var goals = JSON.parse(msg);
            $('.goals').empty();
            $(goals).each(function (index, value) {
                addGoal(value);
            })
        });
    }

    function bindGoalsControl() {
        $('.check-goal').each(function (index, value) {
            $(value).click(function () {
                var goalId = $(this).parent().parent().parent().attr('data-id');
                $.ajax({
                    url: '/todo/check_goal',
                    method: 'POST',
                    data: {
                        goal_id: goalId
                    }
                }).done(function (msg) {
                    var goals = JSON.parse(msg);
                    $('.goals').empty();
                    $(goals).each(function (index, value) {
                        addGoal(value);
                    })
                });
            })
        });
        $('.edit-goal').each(function (index, value) {
            $(value).click(function () {
                var modal = $('#modalWindowGoals');
                var item = $(value).parent().parent().parent();
                modal.find('input[name="title"]').val(item.find('.goal').html());
                modal.find('input[name="date"]').val(item.attr('data-date'));
                modal.find('input[name="goal_id"]').val(item.attr('data-id'));
            })
        });
        $('.edit-category').each(function (index, value) {
            $(value).click(function () {
                var modal = $('#modalWindowCategories');
                var item = $(value).parent().parent().parent();
                modal.find('input[name="category_id"]').val(item.attr('data-id'));
                modal.find('input[name="category"]').val(item.find('a').html());
                modal.find('input[name="color"]').val(item.find('span').css("background-color"));
            })
        });
        $('.delete-goal').each(function (index, value) {
            $(value).click(function () {
                var goalId = $(this).parent().parent().parent().attr('data-id');
                $.ajax({
                    url: '/todo/delete_goal',
                    method: 'POST',
                    data: {
                        goal_id: goalId
                    }
                }).done(function (msg) {
                    var goals = JSON.parse(msg);
                    $('.goals').empty();
                    $(goals).each(function (index, value) {
                        addGoal(value);
                    })
                });
            })
        });
        $('.delete-category').each(function (index, value) {
            $(value).click(function () {
                var categoryId = $(value).parent().parent().parent().attr('data-id');
                $.ajax({
                    url: '/todo/delete_category',
                    method: 'POST',
                    data: {
                        category_id: categoryId
                    }
                }).done(function (msg) {
                    var categories = JSON.parse(msg);
                    $('.categories').empty();
                    $('#chooseCat').html('<option data-content="<i class=\'fa fa-smile-o\' aria-hidden=\'true\'></i>" selected disabled></option>').selectpicker('refresh');
                    $('#chooseCatModal').html('<option data-content="<i class=\'fa fa-smile-o\' aria-hidden=\'true\'></i>" selected disabled></option>').selectpicker('refresh');
                    $(categories).each(function (index, value) {
                        addCategory(value)
                    });
                });
            });
        })
    }

    function bindCategoriesControl() {
        $('.categories .category').each(function (index, value) {
            $(value).click(function () {
                var categoryId = $(this).attr('data-id');
                $.ajax({
                    url: '/todo/get_goals',
                    method: 'POST',
                    data: {
                        category_id: categoryId
                    }
                }).done(function (msg) {
                    var goals = JSON.parse(msg);
                    $('.goals').empty();
                    $(goals).each(function (index, value) {
                        addGoal(value);
                    })
                });
            })
        });
    }

    $('#addCategory').click(function () {
        var category = $(this).parent().find('input[name="category"]').val().trim();
        var color = $(this).parent().find('input[name="color"]').val().trim();
        if (category && color) {
            $.ajax({
                url: '/todo/add_category',
                method: 'POST',
                data: {
                    title: category,
                    color: color
                }
            }).done(function (msg) {
                var categories = JSON.parse(msg);
                $('.categories').empty();
                $('#chooseCat').html('<option data-content="<i class=\'fa fa-smile-o\' aria-hidden=\'true\'></i>" selected disabled></option>').selectpicker('refresh');
                $('#chooseCatModal').html('<option data-content="<i class=\'fa fa-smile-o\' aria-hidden=\'true\'></i>" selected disabled></option>').selectpicker('refresh');
                $(categories).each(function (index, value) {
                    addCategory(value);
                });
            });
        }
    });

    $('#addGoal').click(function () {
        var title = $(this).parent().parent().find('input[name="title"]').val().trim();
        var priority = $(this).parent().parent().find('select[name="priority"] option:selected').val().trim();
        var category = $(this).parent().parent().find('select[name="category"] option:selected').val().trim();
        var date = $(this).parent().parent().find('input[name="date"]').val().trim();
        if (title && priority && category && date) {
            $.ajax({
                url: '/todo/add_goal',
                method: 'POST',
                data: {
                    title: title,
                    priority: priority,
                    category: category,
                    date: date
                }
            }).done(function (msg) {
                var goals = JSON.parse(msg);
                $('.goals').empty();
                $(goals).each(function (index, value) {
                    addGoal(value);
                })
            });
        }
    });

    $('#todayGoals').click(function () {
        getGoalsByTime(0);
    });

    $('#getWeeklyGoals').click(function () {
        getGoalsByTime(1);
    });

    $('#checked').click(function () {
        $.ajax({
            url: '/todo/get_checked_goals',
            method: 'POST'
        }).done(function (msg) {
            var goals = JSON.parse(msg);
            $('.goals').empty();
            $(goals).each(function (index, value) {
                addGoal(value);
            });
        });
    });

    $('#updateGoal').click(function () {
        var title = $(this).parent().parent().find('input[name="title"]').val().trim();
        var priority = $(this).parent().parent().find('select[name="priority"] option:selected').val().trim();
        var category = $(this).parent().parent().find('select[name="category"] option:selected').val().trim();
        var date = $(this).parent().parent().find('input[name="date"]').val().trim();
        var goal_id = $(this).parent().parent().find('input[name="goal_id"]').val().trim();
        if (title && priority && category && date && goal_id) {
            $.ajax({
                url: '/todo/edit_goal',
                method: 'POST',
                data: {
                    goal_id: goal_id,
                    title: title,
                    priority: priority,
                    category_id: category,
                    date: date
                }
            }).done(function (msg) {
                $('#modalWindowGoals').modal('toggle');
                var goals = JSON.parse(msg);
                $('.goals').empty();
                $(goals).each(function (index, value) {
                    addGoal(value);
                })
            });
        }
    });

    $('#updateCategory').click(function () {
        var categoryId = $(this).parent().find('input[name="category_id"]').val().trim();
        var category = $(this).parent().find('input[name="category"]').val().trim();
        var color = $(this).parent().find('input[name="color"]').val().trim();
        if (category && color) {
            $.ajax({
                url: '/todo/edit_category',
                method: 'POST',
                data: {
                    category_id: categoryId,
                    title: category,
                    color: color
                }
            }).done(function (msg) {
                $('#modalWindowCategories').modal('toggle');
                var categories = JSON.parse(msg);
                $('.categories').empty();
                $('#chooseCat').html('<option data-content="<i class=\'fa fa-smile-o\' aria-hidden=\'true\'></i>" selected disabled></option>').selectpicker('refresh');
                $('#chooseCatModal').html('<option data-content="<i class=\'fa fa-smile-o\' aria-hidden=\'true\'></i>" selected disabled></option>').selectpicker('refresh');
                $(categories).each(function (index, value) {
                    addCategory(value);
                });
            });
        }
    });

    bindGoalsControl();
    bindCategoriesControl();
});